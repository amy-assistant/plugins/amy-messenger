IMAGE_NAME ?= registry.gitlab.com/amy-assistant/plugins/amy-messenger
CONTAINER_NAME ?= amy-plugin-messenger
ENV ?= -e app=amy

build: Dockerfile
	docker build -t $(IMAGE_NAME) .

push:
	docker push $(IMAGE_NAME)

run:
	docker run --rm -it --name $(CONTAINER_NAME) $(ENV) $(IMAGE_NAME)

start:
	docker run -d --name $(CONTAINER_NAME) $(ENV) $(IMAGE_NAME)

stop:
	docker stop $(CONTAINER_NAME)

shell:
	docker exec -it $(CONTAINER_NAME) sh

rm:
	docker rm -f $(CONTAINER_NAME)

demo:
	docker run -d --name $(CONTAINER_NAME) $(PORTS) $(ENV) --link amy-q $(IMAGE_NAME)