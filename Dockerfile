FROM python:alpine
RUN apk add --no-cache --virtual .build-deps libressl-dev libffi-dev gcc musl-dev python3-dev 

ENV NAME=messenger
ENV AMY_Q_HOST=159.89.24.207
ENV PRODUCTION=1


RUN pip install amy fbchat

WORKDIR /app

COPY src .

# RUN apk del .build-deps

CMD [ "python", "main.py" ]